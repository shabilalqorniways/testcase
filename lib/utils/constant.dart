import 'dart:ui';

class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

final String apiKey = '24bb05f18d59504807ff121bc1115609';
final String serverUrl = 'https://api.themoviedb.org/3';
final String urlImage = 'https://image.tmdb.org/t/p/w500';

const Color primaryColor = Color(0xFF47bab3);
const Color secColor = Color(0xFFc7e8d7);
const Color thirdColor = Color(0xffffffff);

bool validateEmail(String value) {
  String pattern =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  RegExp regex = RegExp(pattern);
  if (value == null || value.isEmpty || !regex.hasMatch(value))
    return false;
  else
    return true;
}
