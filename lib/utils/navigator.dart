import 'package:flutter/material.dart';

push(BuildContext context, dynamic page) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => page),
  );
}

pop(BuildContext context) {
  Navigator.pop(context);
}

replace(BuildContext context, dynamic page) {
  Navigator.pushAndRemoveUntil(
    context,
    MaterialPageRoute(builder: (context) => page),
    (Route<dynamic> route) => false,
  );
}
