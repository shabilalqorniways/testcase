import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/button.dart';
import 'package:majootestcase/common/widget/text.dart';
import 'package:majootestcase/utils/constant.dart';

Widget errorScreen(BuildContext context, {Function retry}) {
  Size size = MediaQuery.of(context).size;

  return Container(
    height: size.height,
    color: Colors.white,
    padding: EdgeInsets.all(20),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          Icons.warning,
          size: size.width * 0.3,
          color: primaryColor,
        ),
        SizedBox(height: 10),
        CustomText(
          text: 'Network Error!',
          align: TextAlign.center,
          fontSize: 20,
        ),
        SizedBox(height: 20),
        Material(
          child: CustomButton(
            label: 'REFRESH',
            onTap: () {
              retry();
            },
          ),
        ),
      ],
    ),
  );
}
