import 'dart:async';
import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/showLoading.dart';
import 'package:majootestcase/ui/extra/error.dart';
import 'package:majootestcase/providers/connectionProvider.dart';
import 'package:majootestcase/providers/userProvider.dart';
import 'package:majootestcase/ui/beranda/beranda.dart';
import 'package:majootestcase/ui/login/login.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/navigator.dart';
import 'package:majootestcase/utils/localstorage.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SharedPreferencesHandler {
  ConnectionProvider conProvider = ConnectionProvider();
  UserProvider userProvider = UserProvider();
  bool isLoading = true;

  getData() async {
    await userProvider.openDB();
    await userProvider.createUserTable();
    await conProvider.checkConnectivity().then((value) async {
      if (conProvider.isConnect ?? false) {
        int userId = await getUserId();
        if (userId != null) {
          push(context, Beranda());
        } else {
          push(context, Login());
        }
      } else {
        Timer(Duration(seconds: 5), () {
          if (!mounted) return;
          setState(() {
            isLoading = false;
          });
        });
      }
    });
  }

  @override
  void initState() {
    conProvider = Provider.of<ConnectionProvider>(context, listen: false);
    userProvider = Provider.of<UserProvider>(context, listen: false);
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    bool isConnect =
        Provider.of<ConnectionProvider>(context).isConnect ?? false;
    return isLoading
        ? Container(
            width: size.width,
            height: size.height,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment(
                  0.8,
                  0.0,
                ),
                colors: <Color>[
                  primaryColor,
                  secColor,
                ],
              ),
            ),
            child: Center(
              child: Image(
                image: AssetImage('assets/main-logo.png'),
              ),
            ),
          )
        : !isConnect
            ? errorScreen(context, retry: () => getData())
            : circleLoading(context);
  }
}
