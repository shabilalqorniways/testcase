import 'package:flutter/material.dart';
import 'detailMovieView.dart';

class DetailMovie extends StatefulWidget {
  final String adult;
  final String backdrop;
  final String overview;
  final String popularity;
  final String poster;
  final String type;
  final String vote;
  final String year;
  final String title;
  final bool isBack;

  const DetailMovie({
    Key key,
    this.adult,
    this.backdrop,
    this.overview,
    this.popularity,
    this.poster,
    this.type,
    this.vote,
    this.year,
    this.title,
    this.isBack = false,
  }) : super(key: key);

  @override
  DetailMovieView createState() => new DetailMovieView();
}
