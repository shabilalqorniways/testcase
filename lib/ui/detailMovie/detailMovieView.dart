import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/text.dart';
import 'package:majootestcase/common/widget/showLoading.dart';
import 'package:majootestcase/providers/connectionProvider.dart';
import 'package:majootestcase/ui/extra/error.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/navigator.dart';
import 'package:provider/provider.dart';
import '../../models/detailMovieModel.dart';
import 'dart:ui' as ui;

class DetailMovieView extends DetailMovieModel {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    bool isConnect =
        Provider.of<ConnectionProvider>(context).isConnect ?? false;
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: size.width,
          height: size.height,
          child: loading
              ? circleLoading(context)
              : !isConnect
                  ? errorScreen(context)
                  : Stack(
                      fit: StackFit.expand,
                      children: [
                        Image.network(
                          urlImage + widget.backdrop,
                          fit: BoxFit.cover,
                        ),
                        BackdropFilter(
                          filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                          child: Container(
                            color: Colors.black.withOpacity(0.5),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Container(
                            margin: EdgeInsets.all(20.0),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: 400.0,
                                    height: 400.0,
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    image: DecorationImage(
                                      image: NetworkImage(
                                        urlImage + widget.poster,
                                      ),
                                      fit: BoxFit.fitHeight,
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black,
                                        blurRadius: 20,
                                        offset: Offset(0, 10),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 20.0, horizontal: 0.0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        widget.title,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 30.0,
                                            fontFamily: 'Arvo'),
                                      )),
                                      Text(
                                        '${widget.vote}/10',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20.0,
                                          fontFamily: 'Arvo',
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Text(
                                  widget.overview,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Arvo',
                                  ),
                                ),
                                Padding(padding: EdgeInsets.all(10.0)),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        width: 150.0,
                                        height: 60.0,
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Rate Movie',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'Arvo',
                                            fontSize: 20.0,
                                          ),
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: Color(0xaa47bab3),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(16.0),
                                      child: Container(
                                        padding: EdgeInsets.all(16.0),
                                        alignment: Alignment.center,
                                        child: Icon(
                                          Icons.share,
                                          color: Colors.white,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: Color(0xaa47bab3),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Container(
                                          padding: EdgeInsets.all(16.0),
                                          alignment: Alignment.center,
                                          child: Icon(
                                            Icons.bookmark,
                                            color: Colors.white,
                                          ),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            color: Color(0xaa47bab3),
                                          ),
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          top: 10,
                          left: 10,
                          child: InkWell(
                            onTap: () {
                              pop(context);
                            },
                            child: Container(
                              padding: EdgeInsets.all(5),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Color(0xaa47bab3),
                                shape: BoxShape.circle,
                              ),
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        widget.year != "null"
                            ? Positioned(
                                top: 10,
                                right: 10,
                                child: InkWell(
                                  onTap: () {
                                    pop(context);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      right: 10,
                                      left: 10,
                                      top: 5,
                                      bottom: 5,
                                    ),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: Color(0xaa47bab3),
                                      shape: BoxShape.rectangle,
                                    ),
                                    child: CustomText(
                                      text: widget.year,
                                      fontSize: 22,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            : SizedBox(),
                      ],
                    ),
        ),
      ),
    );
  }
}
