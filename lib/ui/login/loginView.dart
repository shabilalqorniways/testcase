import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/button.dart';
import 'package:majootestcase/common/widget/field.dart';
import 'package:majootestcase/common/widget/text.dart';
import 'package:majootestcase/ui/register/register.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/navigator.dart';
import '../../models/loginModel.dart';

class LoginView extends LoginViewModel {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (builderContext) => SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment(
                  0.8,
                  0.0,
                ),
                colors: [
                  Color(0xffe6f1c4),
                  Color(0xFFc7e8d7),
                ],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  width: 200,
                  child: Image(
                    image: AssetImage('assets/main-logo.png'),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: 325,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(15),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        "Selamat Datang",
                        style: TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Silahkan login terlebih dahulu",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Form(
                        key: formKey,
                        child: Column(
                          children: [
                            Container(
                              width: 260,
                              child: CustomField(
                                label: 'Email',
                                hint: 'hallo@gmail.com',
                                isEmail: true,
                                isClearButton: true,
                                onChanged: (value) {
                                  if (!mounted) return;
                                  setState(() {
                                    email = value;
                                  });
                                },
                                validator: (value) {
                                  if (value == '') {
                                    return 'Email tidak boleh kosong';
                                  } else if (!validateEmail(email)) {
                                    return 'Masukkan email yang valid';
                                  }
                                },
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: 260,
                              child: CustomField(
                                label: 'Password',
                                hint: 'password',
                                isPassword: true,
                                onChanged: (value) {
                                  if (!mounted) return;
                                  setState(() {
                                    password = value;
                                  });
                                },
                                validator: (value) {
                                  if (value == '') {
                                    return 'Password tidak boleh kosong';
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CustomButton(
                        label: 'Login',
                        backgroundColor: primaryColor,
                        borderColor: primaryColor,
                        width: 250,
                        onTap: () => onLogin(builderContext),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomText(
                            text: 'Belum punya akun? ',
                          ),
                          InkWell(
                            onTap: () {
                              push(context, Register());
                            },
                            child: CustomText(
                              text: 'Daftar',
                              isBold: true,
                              color: primaryColor,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
