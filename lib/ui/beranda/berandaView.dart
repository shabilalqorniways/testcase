import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/button.dart';
import 'package:majootestcase/common/widget/text.dart';
import 'package:majootestcase/common/widget/listMovieDown.dart';
import 'package:majootestcase/common/widget/showLoading.dart';
import 'package:majootestcase/ui/detailMovie/detailMovie.dart';
import 'package:majootestcase/ui/extra/error.dart';
import 'package:majootestcase/providers/connectionProvider.dart';
import 'package:majootestcase/providers/movieProvider.dart';
import 'package:majootestcase/ui/extra/splash.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/navigator.dart';
import 'package:majootestcase/utils/localstorage.dart';
import 'package:provider/provider.dart';
import '../../models/berandaModel.dart';

class BerandaView extends BerandaModel with SharedPreferencesHandler {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    MovieProvider movie = Provider.of<MovieProvider>(context);
    bool isConnect =
        Provider.of<ConnectionProvider>(context).isConnect ?? false;
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: isLoading
            ? circleLoading(context)
            : !isConnect
                ? errorScreen(
                    context,
                    retry: () => getListMovie(),
                  )
                : SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 20, bottom: 20),
                          width: size.width,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Color(0xffe6f1c4),
                                Color(0xFFc7e8d7),
                              ],
                            ),
                          ),
                          child: CarouselSlider(
                            items: movie.listTrendingWeek.list.map((i) {
                              int index =
                                  movie.listTrendingWeek.list.indexOf(i);
                              return Builder(
                                builder: (BuildContext context) {
                                  return InkWell(
                                    child: Container(
                                      padding:
                                          EdgeInsets.only(left: 10, right: 10),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(20),
                                        ),
                                        child: Stack(
                                          children: <Widget>[
                                            Image.network(
                                              urlImage +
                                                  movie.listTrendingWeek
                                                      .list[index].backdrop,
                                              fit: BoxFit.cover,
                                              width: size.width,
                                              height: 200,
                                            ),
                                            Positioned(
                                              bottom: 0.0,
                                              left: 0.0,
                                              right: 0.0,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  gradient: LinearGradient(
                                                    colors: [
                                                      Color.fromARGB(
                                                          200, 0, 0, 0),
                                                      Color.fromARGB(0, 0, 0, 0)
                                                    ],
                                                    begin:
                                                        Alignment.bottomCenter,
                                                    end: Alignment.topCenter,
                                                  ),
                                                ),
                                                padding: EdgeInsets.symmetric(
                                                  vertical: 15,
                                                  horizontal: 20,
                                                ),
                                                child: Text(
                                                  movie.listTrendingWeek
                                                      .list[index].tittle,
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      push(
                                        context,
                                        DetailMovie(
                                          adult: movie.listTrendingWeek
                                              .list[index].adult,
                                          backdrop: movie.listTrendingWeek
                                              .list[index].backdrop,
                                          overview: movie.listTrendingWeek
                                              .list[index].overview,
                                          popularity: movie.listTrendingWeek
                                              .list[index].popularity,
                                          poster: movie.listTrendingWeek
                                              .list[index].poster,
                                          type: movie.listTrendingWeek
                                              .list[index].type,
                                          vote: movie.listTrendingWeek
                                              .list[index].vote,
                                          year: movie.listTrendingWeek
                                              .list[index].year,
                                          title: movie.listTrendingWeek
                                              .list[index].tittle,
                                          isBack: true,
                                        ),
                                      );
                                    },
                                  );
                                },
                              );
                            }).toList(),
                            autoPlay: true,
                            enlargeCenterPage: true,
                            aspectRatio: 2.0,
                            onPageChanged: (index) {
                              setState(() {
                                currentIndex = index;
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Color(0xFFc7e8d7),
                          width: size.width,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: new BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                ),
                                color: Colors.white,
                              ),
                              padding: EdgeInsets.all(25),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CustomText(
                                      text: "List Video",
                                      fontSize: 25,
                                      isBold: true,
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    listMovieDown(
                                      context,
                                      movie.listTV.list,
                                      'tv',
                                    ),
                                    Center(
                                      child: Container(
                                        width: 200,
                                        child: Padding(
                                          padding: EdgeInsets.all(5),
                                          child: CustomButton(
                                            label: 'Logout',
                                            fontColor: primaryColor,
                                            backgroundColor: Colors.white,
                                            borderColor: primaryColor,
                                            onTap: () {
                                              setUserId(null);
                                              replace(context, SplashScreen());
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
      ),
    );
  }
}
