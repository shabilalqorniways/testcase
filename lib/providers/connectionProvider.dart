import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';

/// Class untuk cek koneksi dalam device
/// Author By Shabil Alqorni
/// Hasil berupa pengecekan koneksi dan status koneksi dalam device
class ConnectionProvider extends ChangeNotifier {
  Connectivity connectivity = Connectivity();
  bool isConnect;

  /// Mengupdate status koneksi sesuai dengan hasil ConnectivityResult
  Future<void> updateConnection(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        isConnect = true;
        break;
      case ConnectivityResult.mobile:
        isConnect = true;
        break;
      case ConnectivityResult.none:
        isConnect = false;
        break;
      default:
        isConnect = false;
        break;
    }
    notifyListeners();

    return isConnect;
  }

  /// Cek koneksi device user
  Future checkConnectivity() async {
    ConnectivityResult result = await connectivity.checkConnectivity();
    await updateConnection(result);
  }

  /// Mengubah status koneksi
  setConnectionStatus(bool status) {
    isConnect = status;
    notifyListeners();
  }
}
