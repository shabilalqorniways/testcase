import 'package:flutter/material.dart';
import 'package:majootestcase/models/class/movieClass.dart';

/// Class untuk memasukkan data dari movie db
/// Author By Shabil Alqorni
/// Hasil berupa manipulasi list data
class MovieProvider extends ChangeNotifier {
  MovieList listMovie = MovieList(); // popular list
  MovieList listTV = MovieList(); // tv list
  MovieList listTrendingToday = MovieList(); // trending list hari ini
  MovieList listTrendingWeek = MovieList(); // trending list minggu ini

  /// Manipulasi data list movie yang popular
  setListMovie(List data) async {
    listMovie = MovieList.fromJson(data);
    notifyListeners();
  }

  /// Manipulasi data list tv
  setListTV(List data) async {
    listTV = MovieList.fromJson(data);
    notifyListeners();
  }

  /// Manipulasi data list movie yang trending hari ini
  setListTrendingToday(List data) async {
    listTrendingToday = MovieList.fromJson(data);
    notifyListeners();
  }

  /// Manipulasi data list movie yang trending pekan ini
  setListTrendingWeek(List data) async {
    listTrendingWeek = MovieList.fromJson(data);
    notifyListeners();
  }
}
