import 'dart:core';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:majootestcase/utils/localstorage.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/utils/utils.dart';

/// Class yang berfungsi untuk login dan register sesuai dengan database lokal
/// Author By Shabil Alqorni
/// Hasil berupa manupulasi data user
class UserProvider extends ChangeNotifier {
  Database db;
  Map dataUser;
  String query;
  // open koneksi ke dalam database
  openDB() async {
    db = await openDatabase('testcase.db');
    notifyListeners();
  }

  // close koneksi database
  closeDB() async {
    await db.close();
    notifyListeners();
  }

  // Membuat table
  createUserTable() async {
    if (!db.isOpen) openDB();
    // Pengecekan table ada atau tidak
    tableExists('users').then(
      (value) async {
        query =
            'CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, email TEXT, password TEXT)';
        if (!value)
          await db.execute(query).then(
            (value) {
              print('value');
            },
          );
      },
    );
    notifyListeners();
  }

  // Pengecekan apakah table sudah ada atau tidak
  Future<bool> tableExists(
    String table, // nama table
  ) async {
    var count = firstIntValue(
      await db.query('sqlite_master',
          columns: ['COUNT(*)'],
          where: 'type = ? AND name = ?',
          whereArgs: ['table', table]),
    );
    return count > 0;
  }

  // Mendapatkan data user
  Future getDataUser(
    String email,
    String password,
  ) async {
    List<Map> list = await db.query(
      'users',
      columns: ['email'],
      where: 'email = ?',
      whereArgs: [email],
    );

    list.where((e) => e['password'] == password).toList();
    if (list.isNotEmpty) {
      dataUser = list[0];
      await SharedPreferencesHandler().setUserId(dataUser['id']);
    }

    notifyListeners();

    return dataUser;
  }

  // Membuat user baru
  insertUser(
    String name,
    String email,
    String password,
  ) async {
    int recordId = await db.insert(
      'users',
      {
        'username': name,
        'email': email,
        'password': password,
      },
    );

    await SharedPreferencesHandler().setUserId(recordId);
    notifyListeners();

    return recordId;
  }
}
