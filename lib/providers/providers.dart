import 'package:majootestcase/providers/connectionProvider.dart';
import 'package:majootestcase/providers/movieProvider.dart';
import 'package:majootestcase/providers/userProvider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

/// List provider yang didaftarkan
List<SingleChildWidget> providerList = [
  ChangeNotifierProvider(create: (_) => UserProvider()),
  ChangeNotifierProvider(create: (_) => MovieProvider()),
  ChangeNotifierProvider(create: (_) => ConnectionProvider()),
];
