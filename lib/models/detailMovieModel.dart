import 'package:flutter/material.dart';
import 'package:majootestcase/providers/connectionProvider.dart';
import 'package:provider/provider.dart';
import '../ui/detailMovie/detailMovie.dart';

abstract class DetailMovieModel extends State<DetailMovie> {
  ConnectionProvider connectionProvider = ConnectionProvider();
  bool loading;

  @override
  void initState() {
    loading = true;
    connectionProvider = Provider.of<ConnectionProvider>(
      context,
      listen: false,
    );
    loading = false;
    super.initState();
  }
}
