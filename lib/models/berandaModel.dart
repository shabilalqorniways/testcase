import 'dart:async';

import 'package:flutter/material.dart';
import 'package:majootestcase/providers/connectionProvider.dart';
import 'package:majootestcase/providers/movieProvider.dart';
import 'package:majootestcase/services/movieService.dart';
import 'package:provider/provider.dart';
import '../ui/beranda/beranda.dart';

abstract class BerandaModel extends State<Beranda> {
  MovieProvider movieProvider = MovieProvider();
  ConnectionProvider connectionProvider = ConnectionProvider();
  bool isLoading = true;

  getListMovie() async {
    if (!mounted) return;
    setState(() {
      isLoading = true;
    });

    await connectionProvider.checkConnectivity().then((value) async {
      if (connectionProvider.isConnect ?? false) {
        await MovieService().getListTV(context);
        await MovieService().getListTrending(context, 'day');
        await MovieService().getListTrending(context, 'week');
        if (!mounted) return;
        setState(() {
          isLoading = false;
        });
      } else {
        Timer(Duration(seconds: 2), () {
          if (!mounted) return;
          setState(() {
            isLoading = false;
          });
        });
      }
    });
  }

  @override
  void initState() {
    getListMovie();
    movieProvider = Provider.of<MovieProvider>(context, listen: false);
    connectionProvider =
        Provider.of<ConnectionProvider>(context, listen: false);
    super.initState();
  }
}
