import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/providers/connectionProvider.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:provider/provider.dart';

/// Class pengaturan Dio
/// Author By Shabil Alqorni
/// Hasil berupa pengaturan post dan get untuk dio
class ApiServices {
  /// Options Dio
  Dio dio = new Dio(
    BaseOptions(
      baseUrl: serverUrl,
      receiveDataWhenStatusError: true,
      connectTimeout: 5000,
      receiveTimeout: 5000,
    ),
  );

  /// GET Method
  Future<Response> getApi(
    BuildContext context,
    String url,
  ) async {
    Response response;
    ConnectionProvider connectionProvider =
        Provider.of<ConnectionProvider>(context, listen: false);

    if (connectionProvider.isConnect ?? false) {
      try {
        response = await dio.get(url);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.CONNECT_TIMEOUT) {
          connectionProvider.setConnectionStatus(false);
        }
        connectionProvider.setConnectionStatus(false);
      }
    }

    return response;
  }

  /// POST Method
  Future<Response> postApi(
    BuildContext context,
    String url,
    Map data,
  ) async {
    Response response;
    ConnectionProvider connectionProvider =
        Provider.of<ConnectionProvider>(context, listen: false);
    if (connectionProvider.isConnect ?? false) {
      try {
        response = await dio.post(
          url,
          data: jsonEncode(data),
        );
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.CONNECT_TIMEOUT) {
          connectionProvider.setConnectionStatus(false);
        }
        connectionProvider.setConnectionStatus(false);
      }
    }
    return response;
  }
}
