import 'package:flutter/material.dart';
import 'package:majootestcase/providers/movieProvider.dart';
import 'package:majootestcase/services/apiService.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:provider/provider.dart';

/// Class untuk memanipulasi data movie
/// Author By Shabil Alqorni
/// Hasil berupa list movie yang tersimpan ke dalam provider
class MovieService {
  /// Get list Tv
  getListTV(BuildContext context) async {
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/tv/popular?api_key=$apiKey',
    )
        .then((value) {
      if (value != null) {
        Provider.of<MovieProvider>(context, listen: false).setListTV(
          value.data['results'],
        );
      }
    });
  }

  /// Get list Trending
  getListTrending(
    BuildContext context,
    String date,
  ) async {
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/trending/all/$date?api_key=$apiKey',
    )
        .then(
      (value) {
        if (value != null) {
          if (date == 'day') {
            Provider.of<MovieProvider>(context, listen: false)
                .setListTrendingToday(
              value.data['results'],
            );
          } else {
            Provider.of<MovieProvider>(context, listen: false)
                .setListTrendingWeek(
              value.data['results'],
            );
          }
        }
      },
    );
  }

  /// Get list Recomendation
  Future getListRecommendation(
    BuildContext context,
    String type,
    int id,
  ) async {
    List response = [];
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/$type/$id/recommendations?api_key=$apiKey',
    )
        .then((value) {
      if (value != null) {
        response = value.data['results'];
      }
    });
    return response;
  }

  /// Pengaturan detail
  Future getDetail(
    BuildContext context,
    String type, // jenis film (movie atau tv)
    int id, // id film
  ) async {
    Map response;
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/$type/$id?api_key=$apiKey',
    )
        .then(
      (value) {
        if (value != null) {
          response = value.data;
        }
      },
    );
    return response;
  }
}
