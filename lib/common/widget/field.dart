import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/text.dart';
import 'package:majootestcase/utils/constant.dart';

class CustomField extends StatefulWidget {
  final String label, hint;
  final Function validator, onChanged;
  final bool isPassword, isEmail, isClearButton;

  const CustomField({
    Key key,
    this.label,
    this.onChanged,
    this.validator,
    this.isPassword = false,
    this.isEmail = false,
    this.hint,
    this.isClearButton = false,
  }) : super(key: key);

  @override
  _CustomFieldState createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isSeePassword = false;
  TextEditingController controller = TextEditingController();

  togglePassword() {
    if (!mounted) return;
    setState(() {
      isSeePassword = !isSeePassword;
    });
  }

  clearField() {
    if (!mounted) return;
    setState(() {
      controller.clear();
    });
  }

  Widget passIcon() {
    return Icon(
      isSeePassword ? Icons.visibility : Icons.visibility_off,
      color: primaryColor,
      size: 20,
    );
  }

  Widget clearIcon() {
    return Icon(
      Icons.clear,
      color: primaryColor,
      size: 20,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            CustomText(
              text: widget.label,
              fontSize: 16,
            ),
          ],
        ),
        SizedBox(height: 5),
        TextFormField(
          controller: controller,
          onChanged: (value) {
            widget.onChanged(value);
          },
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: (value) {
            return widget.validator(value);
          },
          obscureText: widget.isPassword && !isSeePassword,
          keyboardType:
              widget.isEmail ? TextInputType.emailAddress : TextInputType.text,
          decoration: InputDecoration(
            hintText: widget.hint,
            isDense: true,
            suffixIcon: widget.isPassword ||
                    (widget.isClearButton && controller.text.isNotEmpty)
                ? IconButton(
                    onPressed: () =>
                        widget.isPassword ? togglePassword() : clearField(),
                    icon: widget.isPassword ? passIcon() : clearIcon(),
                  )
                : SizedBox(),
            contentPadding: const EdgeInsets.symmetric(
              vertical: 5,
              horizontal: 10,
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: primaryColor,
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(8.0),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: primaryColor,
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(8.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: primaryColor,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      ],
    );
  }
}
