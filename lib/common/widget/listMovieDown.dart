import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/text.dart';
import 'package:majootestcase/models/class/movieClass.dart';
import 'package:majootestcase/ui/detailMovie/detailMovie.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/navigator.dart';

Widget listMovieDown(
  BuildContext context,
  List<MovieData> data,
  String type,
) {
  double width = MediaQuery.of(context).size.width;

  return Column(
      children: data.map((e) {
    return InkWell(
      onDoubleTap: () {
        push(
          context,
          DetailMovie(
            adult: e.adult,
            backdrop: e.backdrop,
            overview: e.overview,
            popularity: e.popularity,
            poster: e.poster,
            type: e.type,
            vote: e.vote,
            year: e.year,
            title: e.tittle,
            isBack: true,
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(
          bottom: 20,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              child: Image.network(
                urlImage + e.poster,
                width: width * 0.2,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    text: e.tittle,
                    maxLines: 2,
                    fontSize: 20,
                  ),
                  SizedBox(height: 5),
                  CustomText(
                    text: "Rating " + e.vote,
                    color: primaryColor,
                  ),
                  SizedBox(height: 5),
                  CustomText(
                    text: e.overview,
                    maxLines: 2,
                    fontSize: 16,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }).toList());
}
