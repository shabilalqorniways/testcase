import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/text.dart';
import 'package:majootestcase/utils/constant.dart';

class CustomButton extends StatefulWidget {
  final String label;
  final Function onTap;
  final double width, height;
  final Color backgroundColor;
  final Color fontColor;
  final Color borderColor;

  CustomButton({
    Key key,
    this.label,
    this.onTap,
    this.width,
    this.height,
    this.backgroundColor = primaryColor,
    this.fontColor,
    this.borderColor = primaryColor,
  }) : super(key: key);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        widget.onTap();
      },
      child: Container(
        width: widget.width ?? 100,
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(50),
          ),
          color: widget.backgroundColor ?? primaryColor,
          border: Border.all(
            width: 1,
            color: widget.borderColor ?? primaryColor,
          ),
        ),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomText(
              text: widget.label,
              isBold: true,
              color: widget.fontColor ?? Colors.white,
              fontSize: 20,
            ),
          ],
        ),
      ),
    );
  }
}
