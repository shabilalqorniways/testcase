# majootestcase

Aplikasi yang ditujukan untuk menuntaskan Test Case: Mobile Flutter Engineer.

## Movie App

Aplikasi ini berisi tentang daftar film dan serial televisi berdasarkan trending dan popular.

## Download Apk

[Aplikasi untuk platform Android]()

## Detail Aplikasi

### Login
Halaman yang membuat user dapat mengakses aplikasi dengan memasukkan informasi terkait akun mereka, yaitu email dan password.

### Beranda
Halaman yang membuat user dapat mendaftarkan akun sehingga user dapat mengakses aplikasi.

### Beranda
Halaman yang berisi daftar film serial televisi berdasarkan trending dan popular.

### Detail Movie
Halaman yang berisi keterangan dari suatu film atau serial televisi dan terdapat daftar rekomendasi film yang terkait.